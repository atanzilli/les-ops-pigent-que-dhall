# Les OPS pigent que Dhall

[The Dhall configuration language](https://dhall-lang.org/)

## Ansible

```bash
$ cd ansible
$ dhall-to-yaml < ansible.dhall > main.yml
```

## Gitlab CI

Uses [dhall-gitlab](https://github.com/advancedtelematic/dhall-gitlab) package.

```bash
$ cd gitlab
$ dhall-to-yaml < gitlab.dhall > .gitlab-ci.yml
```

## Kubernetes

Uses [dhall-kubernetes](https://github.com/dhall-lang/dhall-kubernetes) package.

```bash
$ cd kubernetes
$ dhall-to-yaml < deployment.dhall > deployment.yml
```
