let AptTask =
      { Type = ./types/AptTask.dhall, default = ./defaults/AptTask.dhall }

let InstallDhallTask =
      AptTask::{
      , name = "Ensure dhall is installed"
      , apt = { name = "dhall", state = "present" }
      }

in  [ InstallDhallTask ]
